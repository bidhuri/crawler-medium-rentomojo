const request = require('request');
const cheerio = require('cheerio');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("mediumLinks");
  dbo.createCollection("links", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
    db.close();
  });
});
let q = [];
let threads = 0;
const max = 5;

q.push('https://medium.com/');
var getParams = function (url) {
    var params = [];
    var query = url.split("?")[1];
    //console.log(query);
    if(query!=undefined){
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		params.push(pair[0]);
    }
    }
    return params;
};
function crawlThread() {
    const url = q.shift();
    threads++;
    request(url, (error, response, html) => {
        if (!error) {
            const $ = cheerio.load(html);
            let links = $('a');
            links.each((i, l)=>{
                let t = $(l).attr("href");
                q.push(t);
            })
            let u = url.split("?")[0];
            let p = getParams(url);
            var urlm = "mongodb://localhost:27017/";
            MongoClient.connect(urlm, function(err, db) {
                if (err) throw err;
                var dbo = db.db("mediumLinks");
                dbo.collection("links").findOne({"url":u}, function(err, result) {
                    if (err) throw err;
                    if(result==null){
                        console.log("Nahi Hai:", u, p)
                        dbo.collection("links").insertOne({"url":u, "count":1, "params":p});
                    }
                    else{
                        let ptemp = result.params;
                        let ctemp = result.count;
                        let pt = ptemp.concat(p);
                        let pp = pt.filter(function (item, pos) {return pt.indexOf(item) == pos});
                        console.log("pahle se Hai:", u, p)
                        dbo.collection("links").updateOne({"url":u},{$set:{"count":ctemp+1, "params": pp}});
                    }
                    db.close();
              });
            });
            console.log("Running Threads:", threads)
            while (q.length && threads <= max) {
                crawlThread(); 
            }
            threads--;
        }else{
        	console.error(error);
        }
    });
}

crawlThread();